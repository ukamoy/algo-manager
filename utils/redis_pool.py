from config import db_host, db_port, db_credential, db_hash
import redis

class db_client(object):

    def __init__(self):
        pool = redis.ConnectionPool(host = db_host, port = db_port, decode_responses = True)
        self.db = redis.StrictRedis(connection_pool = pool)
        self.key = db_hash

    def query_one(self, key):
        return self.db.hget(self.key, key)

    def query_many(self, qry, sort = None):
        return self.db.keys(qry)

    def sadd(self, name, value):
        self.db.sadd(name, value)
        self.db.bgsave

    def sunion(self, name):
        return self.db.sunion(name)

    def insert_one(self, key, value):
        self.db.hset(self.key, key, value)
        self.db.bgsave
    
    def insert_many(self, qry):
        self.db.hmset(self.key, qry)
        self.db.bgsave

    def update_one(self, qry, value):
        self.db.hset(self.key, qry, value)
        self.db.bgsave

    def delete_one(self, qry):
        self.db.delete(qry)
    def srem(self, name, value):
        self.db.srem(name, value)