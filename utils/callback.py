from typing import Dict
import json
from utils import exchange

class callback(object):
    def __init__(self, app):
        self.application = app

    def query_strategy(self, wsClient, message: Dict):
        print("stategy callback", message)
        user_id, strategy_type =  message["user_id"], message["strategy_type"]
        stgs = self.application.cache.sunion(f"{user_id}:{strategy_type}")
        strategy_list = []
        for stg_id in stgs:
            strategy = self.application.cache.query_one(f"strategy:{stg_id}")
            if not strategy:
                print("ws: ID  not found:", stg_id)
            else:
                strategy_list.append(json.loads(strategy))
        data = {
            "channel": "strategy",
            "user_id": user_id,
            "strategy_type": strategy_type,
            "data": strategy_list
        }
        wsClient.write_message(data)

    def query_account(self, wsClient, message: Dict):
        print("account callback", message)
        user_id, account =  message["user_id"], message["account"]
        api = self.application.cache.query_one(f"{user_id}:{account}")
        if api:
            api = json.loads(api)            
            gateway = getattr(exchange, api["xchg"])(api)
        equity = []
        r = gateway.query_spot_account(message["instrument"])
        data = {
            "channel": "account",
            "user_id": user_id,
            "account": account,
            "equity": r
        }
        wsClient.write_message(data)