import os
import json
import re
import requests
import hashlib
import hmac
import base64
from urllib.parse import urlencode
from datetime import datetime
import time

OKEX_BASE_URL = "https://www.okex.com"
BITMEX_BASE_URL = "https://testnet.bitmex.com/api/v1"
HUOBI_SPOT_URL = "api.huobi.pro"
HUOBI_FUTURES_URL = "api.hbdm.com"
BINANCE_BASE_URL = "https://api.binance.com"
KUCOIN_TEST_URL = "https://openapi-sandbox.kucoin.com"

class BINANCE(object):
    def __init__(self, account):
        self.apikey = account["apikey"]
        self.secretkey = account["secretkey"].encode()
        self.headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json",
            "X-MBX-APIKEY": self.apikey
        }
        self.time_offset = self.query_time()
        self.symbol_map = self.query_spot_symbols()

    @staticmethod
    def query_spot_symbols():
        r = requests.get(f'{BINANCE_BASE_URL}/api/v1/exchangeInfo', timeout = 10).json()
        symbol_map = {}
        for symbol_info in r.get("symbols",[]):
            symbol_map.update({symbol_info["symbol"]:symbol_info["baseAsset"]})
        return symbol_map

    def query_time(self):
        r = requests.get(f'{BINANCE_BASE_URL}/api/v1/time', timeout = 10).json()
        local_time = int(time.time() * 1000)
        server_time = int(r.get("serverTime", 0))
        return local_time - server_time

    def binance_sign(self, method, path, params = {}):
        timestamp = int(time.time() * 1000)
        if self.time_offset > 0:
            timestamp -= abs(self.time_offset)
        elif self.time_offset < 0:
            timestamp += abs(self.time_offset)
        params["timestamp"] = timestamp

        msg = urlencode(sorted(params.items()))
        signature = hmac.new(self.secretkey,
            msg.encode("utf-8"), hashlib.sha256).hexdigest()

        path = f"{path}?{msg}&signature={signature}"
        return f"{BINANCE_BASE_URL}{path}"

    def query_spot_account(self, currency_pair):
        path = f'/api/v3/account'
        url = self.binance_sign("GET", path)
        r = requests.get(url, headers = self.headers, timeout = 10)
        equity = []
        if r.status_code == 200:
            p = r.json().get("balances", [])
            s = dict(zip(map(lambda x: x["asset"], p), map(lambda x: x["free"], p)))
            for symbol in currency_pair.split("/"):
                d = {
                    "currency": symbol,
                    "available" : s.get(symbol, 0)
                }
                equity.append(d)
            return equity
        else:
            for symbol in currency_pair.split("/"):
                d = {
                    "currency": symbol,
                    "available" : 0
                }
                equity.append(d)
            return equity 

class KUCOIN(object):
    def __init__(self, account):
        self.apikey = account["apikey"]
        self.secretkey = account["secretkey"].encode('utf-8')
        self.passphrase = account["passphrase"]

    def query_spot_account(self, currency_pair = ""):
        path = f"/api/v1/accounts"
        params = {"type":"trade"}
        equity = []
        for symbol in currency_pair.split("/"):
            d = {"currency":symbol}
            params.update({"currency":symbol})
            url, headers = self.kucoin_sign("GET", path, params)
            r = requests.get(url, headers = headers, timeout = 10)
            if r.status_code == 200:
                p = r.json().get("data", [])
                d["available"] = p[0]["available"] if p else 0
            else:
                d["available"] = 0
            equity.append(d)
        return equity

    def kucoin_sign(self, method, path, params = {}):
        body = json.dumps(params) if params and method=="POST" else ""
        path = f"{path}?{urlencode(params)}" if params and method=="GET" else path
        timestamp = str(int(time.time() * 1000))
        msg = f"{timestamp}{method}{path}{body}"
        mac = hmac.new(
            self.secretkey, 
            msg.encode('utf-8'), 
            hashlib.sha256).digest()
        return f"{KUCOIN_TEST_URL}{path}", {
            "KC-API-KEY":self.apikey,
            "KC-API-SIGN":base64.b64encode(mac),
            "KC-API-TIMESTAMP":timestamp,
            "KC-API-PASSPHRASE":self.passphrase
        }