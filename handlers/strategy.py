from handlers import BaseHttpHandler
from typing import Dict
from datetime import datetime
import json

class strategyHandler(BaseHttpHandler):

    def get(self) -> Dict:
        print("get", self.request.__dict__)
        _id = self.get_argument("strategy_id", None)
        stg_info = self.application.cache.query_one(f"strategy:{_id}")
        if stg_info:
            res = {"result": True, "strategy_id":_id, "info":json.loads(stg_info)}
        else:
            res = {"result": False, "strategy_id":_id, "err_msg":f"ID:{_id} not found"}
        self.finish(res)

    def post(self) -> Dict:
        print("post", self.request.__dict__)
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        _id = datetime.today().strftime("%Y%m%d%H%M%S")
        args = [
            "user_id",
            "type",
            "account",
            "strategy_name",
            "exchange",
            "instrument",
            "target_price",
            "quantity",
            "side",
            "participation",
            "trading_mode",
            "end_time"
            ]
        d = {}
        for arg in args:
            d[arg] = self.get_argument(arg, None)
        d["strategy_id"] = _id
        d["status"] = 0
        d["progress"] = 0
        d["filled_qty"] = 0
        d["avg_price"] = 0
        d["latest_trade_price"] = 0
        d["latest_trade_time"] = ""
        self.application.cache.sadd(f"{d['user_id']}:{d['type']}", _id)
        self.application.cache.insert_one(f"strategy:{_id}", json.dumps(d))
        res = {"result": True, "strategy_id":_id, "err_msg":""}
        print(res)
        self.finish(res)

    def put(self) -> Dict:
        print("put", self.request.__dict__)
        args = [
            "user_id",
            "strategy_id",
            "type",
            "account",
            "strategy_name",
            "exchange",
            "instrument",
            "target_price",
            "quantity",
            "side",
            "participation",
            "trading_mode",
            "end_time"
            ]
        d = {}
        for arg in args:
            d[arg] = self.get_argument(arg, None)
        _id = d["strategy_id"]
        if _id:
            strategy = self.application.cache.query_one(f"strategy:{_id}")
            strategy.update(d)
            self.application.cache.update_one(f"strategy:{_id}", json.dumps(strategy))
            res = {"result": True, "strategy_id":_id, "err_msg":""}
        else:
            res = {"result": False, "strategy_id":_id, "err_msg":f"ID:{_id} not found"}
        self.finish(res)


    def delete(self) -> Dict:
        print("delete", self.request.__dict__)
        _id = self.get_argument("strategy_id", None)
        user_id = self.get_argument("user_id", "")
        _type = self.get_argument("type", "")
        self.application.cache.srem(f"{user_id}:{_type}", _id)
        p = self.application.cache.delete_one(f"strategy:{_id}", None)
        if p:
            res = {"result":True, "strategy_id":_id, "err_msg":""}
        else:
            res = {"result":False, "strategy_id":_id, "err_msg": f"ID:{_id} not found"}
        self.finish(res)


handlers = [
    (r"/strategy", strategyHandler),
]