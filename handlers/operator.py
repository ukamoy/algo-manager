from handlers import BaseHttpHandler
from typing import Dict

class operatorHandler(BaseHttpHandler):

    def get(self) -> Dict:
        _id = self.get_argument("strategy_id", None)
        # ans = {}
        # if qry:
        #     ans = self.db.query(_id)
        # self.finish(ans)

handlers = [
    (r"/operator", operatorHandler),
]