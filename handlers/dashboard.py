from handlers import BaseHttpHandler, BaseWsHandler
import json

class dashboardHandler(BaseWsHandler):
    

    def on_message(self, message: str):
        data = json.loads(message)
        self.application.callback.query_strategy(self, data)

handlers = [
    (r"/dashboard", dashboardHandler),
]