from handlers import BaseHttpHandler
from typing import Dict

class loginHandler(BaseHttpHandler):

    def get(self):
        current_user = self.get_current_user()
        if current_user:
            self.redirect("/dashboard")
    def post(self):
        args = {
            "name": self.get_argument("name",None),
            "pwd": self.get_argument("pwd",None),
            "expire": self.get_argument("expire", 1),
        }
        try:
            member = Member()
            member.reload(args['name'], args['pwd'])
            self.clear_all_cookies()
            self.set_cookie(name = "auth", 
                            value = member.auth,
                            expires_days = int(args["expire"]))
            self.redirect("/dashboard")
        except Exception as e:
            print("login error",e)

handlers = [
    (r"/login", loginHandler),

]