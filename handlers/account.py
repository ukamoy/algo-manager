from handlers import BaseHttpHandler, BaseWsHandler
import json
class accountHandler(BaseWsHandler):
    def on_message(self, message: str):
        # {user_id:xx, account_id:xx, instruemnt:xx}
        data = json.loads(message)
        self.application.callback.query_account(self, data)

handlers = [
    (r"/account", accountHandler),
]