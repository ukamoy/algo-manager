import tornado
import tornado.websocket
from utils.redis_pool import db_client
from datetime import datetime
import logging
import json

class BaseHttpHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        member = self.application.cache.query_one("user", {"token": self.get_cookie('token')})
        return self._member_db_map(member) if member else {}

    def _member_db_map(self, data):
        self.user = {
            "name": db.get('name', ""), 
            "group":db.get("group", ""),
            "email" : db.get('email', ""),
            "brief" : db.get('brief', ""),
            "verified" : db.get('verified', ""),
            "contacter" : db.get('contacter', ""),
            }
        logging.info(f"{self.user['name']},{self.request.uri},{self.request.remote_ip}")
        return self.user

class BaseWsHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        logging.debug(f"new connection, {self.request.uri}, {self.request.remote_ip}")
        # self.write_message(json.dumps({"result":True}))

    def on_close(self):
        logging.debug(f"connection closed, {self.request.uri}, {self.request.remote_ip}")

    def on_error(self, error: str):
        logging.debug(error)