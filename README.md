# algo-manager

### 接口说明：
> Host
##### 172.16.11.88:9998
<!-- > Pre
#### 1、 RESTful 请求头必须带 token 用于用户验证
#### 2、 WebSocket 在第一次连接时需要鉴权 -->
> Detail
##### 1、用户登陆接口 Login
    POST /login
    body {  
        "user": string,              
        "pwd": string,
        "expire": int
        }
    return {"token": hash#}
    说明：expire 为 token 的有效期, 单位为天的整型

##### 2、 算法配置接口 Strategy
    > 任务编辑页提交新的算法配置
    POST /strategy
    body {  
        "user_id": str,            // 用户ID
        "type": string,            // 策略类型
        "account": string,         // 账户
        "strategy_name": string,   // 任务名
        "instrument": string,      // 币对
        "target_price": int,       // 目标价格
        "quantity": int,           // 数量
        "side": int,               // 买卖方向, 1 = 买入, -1 = 卖出
        "participation": float,    // 参与度, 0 为动态参与度, 其他值为固定参与度
        "trading_mode": int,       // 发单模式, 1 = 主动, 0 = 被动
        "end_time": string         // 停止时间
        }
    return {
        "result": bool,            // 提交结果
        "strategy_id": int,        // 策略ID
        "err_msg": str             // 如果提交失败, 返回错误码
    }

    > 任务编辑页获取用于修改的算法配置
    GET /strategy
    body {"strategy_id": int}
    return {
        "result": bool,
        "strategy_id": int,
        "info":{
            "type": string,
            "account": string,
            "strategy_name": string,
            "instrument": string,
            "target_price": int,
            "quantity": int,
            "side": string,
            "participation": float,
            "trading_mode": string,
            "end_time": string
            }
        }

    > 任务编辑页保存算法配置的更新
    PUT /strategy
    body {  
        "type": string,
        "account": string,
        "strategy_id": int,
        "strategy_name": string,
        "instrument": string,
        "target_price": int,
        "quantity": int,
        "side": string,
        "participation": float,
        "trading_mode": string,
        "end_time": string
        }
    return {
        "result": bool,
        "strategy_id": int,
        "err_msg": str
    }

    > 任务编辑页删除算法配置
    DEL /strategy
    body {"user_id": str, "strategy_id": int, "type": str}
    return {
        "result": bool,
        "strategy_id": int,
        "err_msg": dict
    }

##### 3、 策略操作接口 Operator
    POST /operator
    body {
        "strategy_id": int,             // 策略ID
        "flag": int                     // 操作指令, 1 = 启动, 0 = 停止
        }
    return {"result": bool, "strategy_id": int, "err_msg": string}

##### 4、 账户资金接口 Account
    WS /account
    send {
        "user_id: str,
        "account": str,
        "instrument": str               //货币对
        }
    recv {
        "channel":"account",
        "user_id": str,
        "account": str,
        "equity":[
            {
                "currency": string,     // 货币
                "available": float      // 可用资金
            },
        ]
    }

##### 5、 策略展示接口 Dashboard
    WS /dashboard 
    send {
        "user_id": str,
        "strategy_type": str,
        "limit": int,
        "page": int
        }

    recv {
        "channel":"strategy",
        "user_id": str,
        "strategy_type": str, 
        "data":[
             {
                "strategy_id": int,             // 策略ID
                "strategy_name": string,        // 策略名称
                "status": int,                  // 策略状态
                "instrument": string,           // 币对
                "side": string,                 // 买卖方向, 1 = 买, -1 = 卖
                "progress": float,              // 成交比例
                "filled_qty": int,              // 已成交数量
                "quantity": int,                // 委托总数量
                "avg_price": float,             // 成交均价
                "latest_trade_price": float,    // 最新一笔委托价格
                "latest_trade_time": string     // 最新一笔委托时间
            },
        ]
    }