import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line
import os
import urls
from utils.redis_pool import db_client
from utils.callback import callback

options.log_file_prefix = os.path.join(os.path.dirname(__file__), "logs/tornado.log")
options.logging = "debug"
options.log_rotate_mode = "time"
options.log_rotate_when = "D"
options.log_to_stderr = True
# options.log_file_num_backups = 5
settings = {
    # "static_path": os.path.join(os.path.dirname(__file__), "static"),
    # 'template_path': os.path.join(os.path.dirname(__file__), "templates"),
    "login_url": "/",
    "debug": True
}

# 配置tornado web应用
class Application(tornado.web.Application):
    def __init__(self):
        self.cache = db_client()
        self.callback = callback(self)
        tornado.web.Application.__init__(self, urls.handlers, **settings)

if __name__ == "__main__":
    app = Application()
    app.listen(9998)
    parse_command_line()
    tornado.ioloop.IOLoop.current().start()