from handlers import dashboard, account, strategy, operator, users

handlers=[]
handlers.extend(dashboard.handlers)
handlers.extend(account.handlers)
handlers.extend(strategy.handlers)
handlers.extend(operator.handlers)
handlers.extend(users.handlers)